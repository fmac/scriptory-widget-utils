const syURI = 'https://scriptory.io';

const respondeCodes = {
    3000: 'Store file fetched.',
    4000: 'Error while fetching store file.'
};

const widgetUtils = (function () {
    let port = null;
    let portReadyCallback = null;
    let inputCallback = null;
    let storageCallback = null;

    function messageReceived(evt) {
        if (evt.origin !== syURI) {
            return;
        }
        switch (evt.data.type) {
            case 'initPort':
                port = evt.ports[0];
                port.onmessage = messageFromPortReceived;
                if (portReadyCallback) {
                    portReadyCallback();
                }

                break;
            case 'storage':
                if (storageCallback) {
                    storageCallback(evt.data.content);
                }
                break;
            default:
                break;
        }
    }

    function messageFromPortReceived(evt) {
        if (inputCallback) {
            inputCallback(evt.data.source, evt.data.content);
        }
    }

    function portReady(callback) {
        portReadyCallback = callback;
        if (port) {
            callback();
        }
    }

    function input(callback) {
        inputCallback = callback;
    }

    function output(content = null) {
        if (!port) {
            return false;
        }
        port.postMessage({ type: 'output', content });

        return true;
    }

    function fetchStorage(callback) {
        storageCallback = callback;

        if (!port) {
            return false;
        }
        port.postMessage({ type: 'fetchStorage' });

        return true;
    }

    function saveStorage(data) {
        if (!port) {
            return false;
        }
        port.postMessage({ type: 'saveStorage', content: data });

        return true;
    }

    window.addEventListener('message', messageReceived);

    return {
        portReady,
        output,
        input,
        fetchStorage,
        saveStorage
    };
})();
